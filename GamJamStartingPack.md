

# Game Jam Starting Pack

----------

## Intro :

4:44 Rule - Rami Ismail (Vlambeer) - Nordic Game Jam 2016 [78h] 
4h for creation, 44 for polishing
https://www.youtube.com/watch?v=lPyYZjCQ0Is


Game Jam Tricks And Tips [Art & Sound]
https://www.youtube.com/watch?v=86UHZq59HKo
https://www.youtube.com/watch?v=yXmaH4h-4bA


3 Steps For Developing A Simple Game Environment - AssetForge + Unreal Engine 4
https://www.youtube.com/watch?v=3hCYwgBilCI


How to Keep Players Engaged (Without Being Evil) | Game Maker's Toolkit
https://www.youtube.com/watch?v=goSASGwE03k


30 Second Quest Week 1 - Game Jam Behind The Scenes [Indie Game Devlog]
https://www.youtube.com/watch?v=2txoJ4JKC74&t=699s&index=111&list=PLa5gVw8WO5oZ0NDIBfzkQSMnVUUH-eH75


A Game In 48 Hours - Ludum Dare 39 Timelapse & Post Mortem - Robot Power by Imphenzia
https://www.youtube.com/watch?v=pSCYn3dQX_4&t=0s&list=PLa5gVw8WO5oZ0NDIBfzkQSMnVUUH-eH75&index=89

Ludum Dare 36 Timelapse
https://www.youtube.com/watch?v=zR21dEXtKyg


-----------

## Basic Unity tuts : 

Roll-a-Ball
https://unity3d.com/learn/tutorials/s/roll-ball-tutorial

2D Game Creation
https://unity3d.com/learn/tutorials/s/2d-game-creation

Survival Shooter
https://unity3d.com/learn/tutorials/s/survival-shooter-tutorial


--------

## Courses

Game Design: Art and Concepts 
https://www.coursera.org/specializations/game-design

Game Design and Development 
https://www.coursera.org/specializations/game-development

Concepts in Game Development (GamesDev)
https://www.open2study.com/courses/concepts-in-game-development



## 3D

Blender Character Creation: Modelling
https://www.youtube.com/watch?v=DiIoWrOlIRw&index=64&list=PLa5gVw8WO5oZ0NDIBfzkQSMnVUUH-eH75&t=0s



How To Pixel Art Tutorial Part 11: Character Design
https://www.youtube.com/watch?v=y5JEc-A8PzY&index=63&list=PLa5gVw8WO5oZ0NDIBfzkQSMnVUUH-eH75&t=0s



## Design


The Design in Narrative Design
https://www.youtube.com/watch?v=f8VIlfTtypg&t=255s&index=213&list=PLa5gVw8WO5oZ0NDIBfzkQSMnVUUH-eH75


Super Mario 3D World's 4 Step Level Design | Game Maker's Toolkit   
https://www.youtube.com/watch?v=dBmIkEvEBtA



------------------------------------------
------------------------------------------

## Agile/Jam workflow :    
Agile - flexible workflow   
Scrum - standup  15 min + frequent builds    
Backlog    
Sprint 4h + standups   
Retrospective   

Roles :    
Product owner   
Scrum master    
Team priorities   
Minimum Valuable Product  
Cut useless features   

Constant rechecks and cycles   
Flow may change during the process (instead of requirements)   

Kanban board (everyone takes what he want, max N tasks + taking into account a priority level !) (e.g. 2 in progress per person)   

4 - 5 сolumns  
Who has time - close tasks / check and re-assign new -/- or return some tasks to prev column  

---------------------

## Jam workflow adaptation    
Brainstorm   
Backlog     
Shot sprints for 4h   
(Each sprint some guy should spend his time on task management and checking QC!)     
Breaks with retrospective    
New goals/checks backlog     

The week before the jam - try to implement maximum features from the list (some list), at least 70% + several prototypes, models, levels - etc   

---------------------------

## Producer / Scrum master   

Controls workflow     
Control Agile     
Critical decisions (which are not applicable to design)  
Manager / Helper   
Task manager      
Timelines    

----

## Game Designer    
Game flow  
Game mechanics     
Game balance (Google SpreadSheets, EXEL for example)   
Hp points, Damage, hardness, collision/object size   
Balance Tweaks   
Balance testing  
Play cycels  
Rules  - a matrix of interaction  
Win/Lose condition,  increase rules   
Player UX  

Tutorial  for player, examples   
Set goals for the player  

------------------

## Programmer    
Game rules implementation (observer)  
Gameplay programming   
Start / End condition   
Playable gameplay prototype  

A transition between screens/levels     
Splash screen  

Player control  
Environment interaction (player enemy NPC obstacles triggers) (level designer is also involved)    
Score / health control  
Ui/Hud control    

AI agents and their behavior with movement      

Game Modes     
Build/ sync process    
Sound/music events    
Debugging/bug fixing    
Physics    

Spawns / Lifetime destruction   
Optimization   
Graphical Configs (polishing)  

Asset initialization   

-------------------------------

## Level Designer
Environment story  
Art style   
Level Borders  

Golden path 
Comfort first, decor later (again - check multiplayer maps)  

Spawn points / Spawn balance  
Obstacles  

Level maze (look at maps from competitive games, GDC talks)   
Interactive objects, parts of the environment - blueprints, lamps, switches, buttons, traps, health, coins, bonuses   
Level editor   

Playtests  
Level load and permanence optimization  
  
Lighting, points of light, sun etc.

--------------------

## VFX artist  
Partical effects,   
Level mood and sparks   
Lighting effects  


---------------------------

## Narrative designer   
Story, Explanation  
A story, backstory, background   
Narrator   

Textes, dialogues    
Characters, plot twists :D  
NPC back story    
placeholder textes   

---------------------------


## Sound designer / Fx / Musician   
Sound effects (.wav is better)   
Sfx editor, Music tracker   

Ambient/flow music   
Free music or well-known OSTs   
Interaction music (walk, explosions, fire/shooting, punches, alarm clock, voices)   
Main menu music   

Audio editing (sunvox, 8bit editors, audacity)    
Youtube script for audio export (animal sounds, etc)   
Voice placeholders   

UI/Menu sound feedback   

-------------------

## 3D Artists   
At least Low poly characters    
Duplicate with a decreased amount of polygons for in-game collider   

Rigs/Bones   
Materials   
UV mapping    
Animations for different actions (run, jump,  punch)   
An export process to the game engine    

3d Environment objects (bushes, weapons, rocks)   
Pieces of level    
Environment objects   
Generate normal / bump maps   

-------------------

## 2D Artists   
Logo with a team name, typography   
Default game logo   
UI buttons   
HUD icons   
The game icon for .exe   
Screenshot software   

Generate textures    
2D sprite character( it can be pixel art )    
Sprite animation frames grid     
Skyboxes


-----------------------

## QA/QC    
Blocker/Crashes searcher   
Cross-Platform tests   
Advice about polishing   
Controls, resolutions.   
Advice about balance   

------------------


## Marketing person    
Advertise in social media, jam blog-glow, discord charts
Marketing textes
Video, Screenshots, Gifs
Comments, feedback to other games


-----------------------------------------
-----------------------------------------

# Flows to develop :   
Texture creation -> transfering to Engine material (from texture)   
2D UI creation -> trasfering to Game UI   
3D assets creation -> rigging -> animations -> transfer to engine and apply -> triggers for animation sequences  



--------------------------------------------------
-----------------------------------------

# Jam examples :  
Best Ludum Dare 40 Games #9: Birds Birds Birds, Curse of Greed, Burrowed Time, Horny Bunny, Pok3d
https://www.youtube.com/watch?v=uBsKVWvXcRA

The best games from GMTK Game Jam 2017
https://www.youtube.com/watch?v=yDmjt-UfybM